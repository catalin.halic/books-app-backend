package com.sda.firstproject;

import com.fasterxml.jackson.core.JsonEncoding;
import com.sda.firstproject.books.Book;
import com.sda.firstproject.books.BookRepositoryImpl;
import com.sda.firstproject.users.User;
import com.sda.firstproject.utils.Constants;
import com.sda.firstproject.utils.ResponseHandler;
import org.apache.tomcat.util.json.JSONParser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.json.JsonParser;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SpringBootApplication
@RestController
public class FirstprojectApplication {
    static BookRepositoryImpl bookRepository;

    public static void main(String[] args) {
        bookRepository = new BookRepositoryImpl();

        SpringApplication.run(FirstprojectApplication.class, args);
    }

    @CrossOrigin(origins = Constants.CROSS_ORIGIN)
    @PostMapping("/auth/login")
    public ResponseEntity<String> login(@RequestBody User user) {
        System.out.println("Login request");
        System.out.println(user.getUsername());
        System.out.println(user.getPassword());

        String body = ResponseHandler.createResponse(200, "Bun venit " +user.getUsername() );

         return ResponseEntity.ok()
                .body(body);
    }

    @CrossOrigin(origins = Constants.CROSS_ORIGIN)
    @PostMapping("/auth/register")
    public ResponseEntity<String> register(@RequestBody User user) {
        System.out.println("Register request");
        System.out.println(user.getUsername());
        System.out.println(user.getPassword());
        System.out.println(user.getConfirmPassword());

        String body = ResponseHandler.createResponse(200, "cont creat cu succes pentru: " + user.getUsername());
        return ResponseEntity.ok()
                .body(body);
    }

    @CrossOrigin(origins = Constants.CROSS_ORIGIN)
    @GetMapping("/books")
    public List<Book> findAllBooks() {
        return bookRepository.findAll();
    }

    @CrossOrigin(origins = Constants.CROSS_ORIGIN)
    @PostMapping("/books")
    public void addBook(@RequestBody Book book) {
        bookRepository.addBook(book);
    }

    @CrossOrigin(origins = Constants.CROSS_ORIGIN)
    @PutMapping("/books")
    public void updateBook(@RequestParam(value="id") String id, @RequestBody Book book) {
        bookRepository.updateBook(id, book);
    }

    @CrossOrigin(origins = Constants.CROSS_ORIGIN)
    @DeleteMapping("/books")
    public void deleteBook(@RequestParam(value="id") String id) {
        bookRepository.deleteBook(id);
    }

    @CrossOrigin(origins = Constants.CROSS_ORIGIN)
    @GetMapping("/books/byId")
    public Book getBookById(@RequestParam(value="id") String id) {
        return bookRepository.findById(id);
    }

    @CrossOrigin(origins = Constants.CROSS_ORIGIN)
    @GetMapping("/books/byAuthor")
    public List<Book> getBookByAuthor(@RequestParam(value="author") String author) {
        return bookRepository.findByAuthor(author);
    }

    @CrossOrigin(origins = Constants.CROSS_ORIGIN)
    @GetMapping("/books/byTitle")
    public List<Book> getBookByTitle(@RequestParam(value="title") String title) {
        return bookRepository.findByTitle(title);
    }

    @CrossOrigin(origins = Constants.CROSS_ORIGIN)
    @GetMapping("/books/byDescription")
    public List<Book> getBookByDescription(@RequestParam(value="description") String description) {
        return bookRepository.findByDescription(description);
    }

}
