package com.sda.firstproject.books;

import org.springframework.stereotype.Repository;

import java.util.*;

public class BookRepositoryImpl implements BooksRepository {
    private HashMap<Integer, Book> bookHashMap;
    private int contor;

    public BookRepositoryImpl() {
        this.bookHashMap = new HashMap<>();
        this.contor = 1;
    }

    @Override
    public List<Book> findByAuthor(String authorName) {
        List<Book> bookList = new ArrayList<>();

        for(Map.Entry<Integer, Book> entry : bookHashMap.entrySet()) {
            Integer key = entry.getKey();
            // element from hashmap
            Book bk = bookHashMap.get(key);

            // check if the book author name contains the authorName(parameter) value
            if(bk.getAuthor().toLowerCase(Locale.ROOT)
                    .contains(authorName.toLowerCase(Locale.ROOT))) {
                bookList.add(bk);
            }
        }

        return bookList;
    }

    @Override
    public List<Book> findByTitle(String title) {
        List<Book> bookList = new ArrayList<>();

        for(Map.Entry<Integer, Book> entry : bookHashMap.entrySet()) {
            Integer key = entry.getKey();
            // element from hashmap
            Book bk = bookHashMap.get(key);

            // check if the book author name contains the authorName(parameter) value
            if(bk.getTitle().contains(title)) {
                bookList.add(bk);
            }
        }

        return bookList;
    }

    @Override
    public List<Book> findByDescription(String description) {
        List<Book> bookList = new ArrayList<>();

        for(Map.Entry<Integer, Book> entry : bookHashMap.entrySet()) {
            Integer key = entry.getKey();
            // element from hashmap
            Book bk = bookHashMap.get(key);

            // check if the book author name contains the authorName(parameter) value
            if(bk.getDescription().contains(description)) {
                bookList.add(bk);
            }
        }

        return bookList;
    }

    @Override
    public Book findById(String id) {
        for(Map.Entry<Integer, Book> entry : bookHashMap.entrySet()) {
            Integer key = entry.getKey();
            // element from hashmap
            if(key == Integer.parseInt(id)) {
                return bookHashMap.get(key);
            }
        }

        return null;
//        return this.bookHashMap.get(Integer.parseInt(id));
    }

    @Override
    public List<Book> findAll() {
        List<Book> bookList = new ArrayList<>();

        

        for(Map.Entry<Integer, Book> entry : bookHashMap.entrySet()) {
            Integer key = entry.getKey();

            // element from hashmap
            Book bk = bookHashMap.get(key);

            bookList.add(bk);
        }

        return bookList;
    }

    @Override
    public void addBook(Book book) {
        // id - is class variable and start with 1
        // id is used as key for hashmap
        book.setId(this.contor);

        this.bookHashMap.put(this.contor, book);

        this.contor++;
    }

    @Override
    public void updateBook(String id, Book book) {
        if(this.bookHashMap.containsKey(Integer.parseInt(id))) {
            this.bookHashMap.put(Integer.parseInt(id), book);
        }
    }

    // "2"
    // "doi"
    @Override
    public void deleteBook(String id) {
        if(this.bookHashMap.containsKey(Integer.parseInt(id))) {
            this.bookHashMap.remove(Integer.parseInt(id));
        }

    }
}
