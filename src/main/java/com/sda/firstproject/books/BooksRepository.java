package com.sda.firstproject.books;

import java.util.List;

public interface BooksRepository {
    List<Book> findByAuthor(String authorName);

    List<Book> findByTitle(String title);

    List<Book> findByDescription(String description);

    Book findById(String id);

    List<Book> findAll();

    void addBook(Book book);

    void updateBook(String id, Book book);

    void deleteBook(String id);
}
