package com.sda.firstproject.utils;

public class ResponseHandler {

    public static String createResponse(int statusCode, String message) {
        StringBuilder stringBuilder = new StringBuilder();
        // { "statusCode": 200, "message": "Bun venit" }

        stringBuilder.append("{");

        stringBuilder.append("\"").append("statusCode").append("\"").append(":\"").append(statusCode).append("\",");
        stringBuilder.append("\"").append("message").append("\"").append(":\"").append(message).append("\"");

        stringBuilder.append("}");

        return stringBuilder.toString();

    }
}
